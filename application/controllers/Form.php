<?php

class Form extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('m_form');
        $this->load->helper('url');
        $this->load->library('pagination');
    }

    function index(){
        //konfigurasi pagination
        $config['base_url'] = base_url('Form/index'); //site url
        $config['total_rows'] = $this->db->count_all('biodata'); //total row
        $config['per_page'] = 1;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
        // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->m_form->get_kuesioner_list($config["per_page"], $data['page']);           
 
        $data['pagination'] = $this->pagination->create_links();
        $data['biodata'] = $this->m_form->tampil()->result();
        $this->load->view('v_tampil',$data);
    }

    function tambah_data(){
        $this->load->view('v_form');
    }

    function tambah_aksi(){
        $id_bio = $this->input->post('id_bio');
        $judul = $this->input->post('judul');
        $nama_ketua = $this->input->post('nama_ketua');
        $waktu = $this->input->post('waktu');
        $lokasi = $this->input->post('lokasi');
        $nama_dpl = $this->input->post('nama_dpl');
        $pejabat_desa = $this->input->post('pejabat_desa');
        $dpl_terlibat = $this->input->post('dpl_terlibat');
        $dpl_antar = $this->input->post('dpl_antar');
        $dpl_aktif = $this->input->post('dpl_aktif');
        $log_book = $this->input->post('log_book');
        $jadwal_pelaksanaan = $this->input->post('jadwal_pelaksanaan');
        $daftar_harian = $this->input->post('daftar_harian');
        $kekompakan = $this->input->post('kekompakan');
        $permasalahan = $this->input->post('permasalahan');
        $penyediaan_posko = $this->input->post('penyediaan_posko');
        $partisipasi_mas = $this->input->post('partisipasi_mas');
        $desa_butuh = $this->input->post('desa_butuh');
        $prog_unggulan =  implode(',', $this->input->post('prog_unggulan',TRUE));
        $masalah_desa = implode(',', $this->input->post('masalah_desa',TRUE));
        $harapan = $this->input->post('harapan');

        $data = array(
            'id_bio' => $id_bio,
            'judul' => $judul, 
            'nama_ketua' => $nama_ketua,
            'waktu' => $waktu,
            'lokasi' => $lokasi,
            'nama_dpl' => $nama_dpl,
            'pejabat_desa' => $pejabat_desa,
            'dpl_terlibat' => $dpl_terlibat,
            'dpl_antar' => $dpl_antar,
            'dpl_aktif' => $dpl_aktif,
            'log_book' => $log_book,
            'jadwal_pelaksanaan' => $jadwal_pelaksanaan,
            'daftar_harian' => $daftar_harian,
            'kekompakan' => $kekompakan,
            'permasalahan' => $permasalahan,
            'penyediaan_posko' => $penyediaan_posko,
            'partisipasi_mas' => $partisipasi_mas,
            'desa_butuh' => $desa_butuh,
            'prog_unggulan' => $prog_unggulan,
            'masalah_desa' => $masalah_desa,
            'harapan' => $harapan
        );
        $this->m_form->input($data,'biodata');
        redirect('Form/index');
    }

    function hapus($id_bio){
        $where = array('id_bio' => $id_bio);
        $this->m_form->hapus_data($where,'biodata');
		redirect('Form/index');

    }

    function edit($id_bio){
        $where = array('id_bio' => $id_bio);
        $data['biodata'] = $this->m_form->edit_data($where,'biodata')->result();
        $this->load->view('v_edit',$data);
    }

    function update(){
        $id_bio = $this->input->post('id_bio');
        $judul = $this->input->post('judul');
        $nama_ketua = $this->input->post('nama_ketua');
        $waktu = $this->input->post('waktu');
        $lokasi = $this->input->post('lokasi');
        $nama_dpl = $this->input->post('nama_dpl');
        $pejabat_desa = $this->input->post('pejabat_desa');
        $dpl_terlibat = $this->input->post('dpl_terlibat');
        $dpl_antar = $this->input->post('dpl_antar');
        $dpl_aktif = $this->input->post('dpl_aktif');
        $log_book = $this->input->post('log_book');
        $jadwal_pelaksanaan = $this->input->post('jadwal_pelaksanaan');
        $daftar_harian = $this->input->post('daftar_harian');
        $kekompakan = $this->input->post('kekompakan');
        $permasalahan = $this->input->post('permasalahan');
        $penyediaan_posko = $this->input->post('penyediaan_posko');
        $partisipasi_mas = $this->input->post('partisipasi_mas');
        $desa_butuh = $this->input->post('desa_butuh');
        $prog_unggulan = $this->input->post('prog_unggulan');
        $masalah_desa = implode(',', $this->input->post('masalah_desa',TRUE));
        $harapan = $this->input->post('harapan');
       
        $data = array(
            'id_bio' => $id_bio,
            'judul' => $judul, 
            'nama_ketua' => $nama_ketua,
            'waktu' => $waktu,
            'lokasi' => $lokasi,
            'nama_dpl' => $nama_dpl,
            'pejabat_desa' => $pejabat_desa,
            'dpl_terlibat' => $dpl_terlibat,
            'dpl_antar' => $dpl_antar,
            'dpl_aktif' => $dpl_aktif,
            'log_book' => $log_book,
            'jadwal_pelaksanaan' => $jadwal_pelaksanaan,
            'daftar_harian' => $daftar_harian,
            'kekompakan' => $kekompakan,
            'permasalahan' => $permasalahan,
            'penyediaan_posko' => $penyediaan_posko,
            'partisipasi_mas' => $partisipasi_mas,
            'desa_butuh' => $desa_butuh,
            'prog_unggulan' => $prog_unggulan,
            'masalah_desa' => $masalah_desa,
            'harapan' => $harapan
        );
    
        $where = array(
            'id_bio' => $id_bio
        );
    
        $this->m_form->update_data($where,$data,'biodata');
        redirect('Form/index');
    }
}