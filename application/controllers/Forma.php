<?php

class Forma extends CI_Controller{


    function __construct(){
        parent::__construct();
        $this->load->model('m_forma');
        $this->load->helper('url');
        

    }

    function index(){
        //konfigurasi pagination
        $data['model'] = $this->m_forma->tampil(); // Panggil fungsi view() yang ada di model siswa        
        $this->load->view('v_tampila', $data);
    }

    function tambah_data(){
        $this->load->view('v_form');
    }

    function tambah_aksi(){
        $id_bio = $this->input->post('id_bio');
        $judul = $this->input->post('judul');
        $nama_ketua = $this->input->post('nama_ketua');
        $waktu = $this->input->post('waktu');
        $lokasi = $this->input->post('lokasi');
        $nama_dpl = $this->input->post('nama_dpl');
        $pejabat_desa = $this->input->post('pejabat_desa');
        $dpl_terlibat = $this->input->post('dpl_terlibat');
        $dpl_antar = $this->input->post('dpl_antar');
        $dpl_aktif = $this->input->post('dpl_aktif');
        $log_book = $this->input->post('log_book');
        $jadwal_pelaksanaan = $this->input->post('jadwal_pelaksanaan');
        $daftar_harian = $this->input->post('daftar_harian');
        $kekompakan = $this->input->post('kekompakan');
        $permasalahan = $this->input->post('permasalahan');
        $penyediaan_posko = $this->input->post('penyediaan_posko');
        $partisipasi_mas = $this->input->post('partisipasi_mas');
        $desa_butuh = $this->input->post('desa_butuh');
        $prog_unggulan =  implode(',', $this->input->post('prog_unggulan',TRUE));
        $masalah_desa = implode(',', $this->input->post('masalah_desa',TRUE));
        $harapan = $this->input->post('harapan');

        $data = array(
            'id_bio' => $id_bio,
            'judul' => $judul, 
            'nama_ketua' => $nama_ketua,
            'waktu' => $waktu,
            'lokasi' => $lokasi,
            'nama_dpl' => $nama_dpl,
            'pejabat_desa' => $pejabat_desa,
            'dpl_terlibat' => $dpl_terlibat,
            'dpl_antar' => $dpl_antar,
            'dpl_aktif' => $dpl_aktif,
            'log_book' => $log_book,
            'jadwal_pelaksanaan' => $jadwal_pelaksanaan,
            'daftar_harian' => $daftar_harian,
            'kekompakan' => $kekompakan,
            'permasalahan' => $permasalahan,
            'penyediaan_posko' => $penyediaan_posko,
            'partisipasi_mas' => $partisipasi_mas,
            'desa_butuh' => $desa_butuh,
            'prog_unggulan' => $prog_unggulan,
            'masalah_desa' => $masalah_desa,
            'harapan' => $harapan
        );
        $this->m_forma->input($data,'biodata');
        redirect('Forma/index');
    }

    function hapus($id_bio){
        $where = array('id_bio' => $id_bio);
        $this->m_forma->hapus_data($where,'biodata');
		redirect('Forma/index');

    }

    function edit($id_bio){
        $where = array('id_bio' => $id_bio);
        $data['biodata'] = $this->m_forma->edit_data($where,'biodata')->result();
        $this->load->view('v_edit',$data);
    }

    function update(){
        $id_bio = $this->input->post('id_bio');
        $judul = $this->input->post('judul');
        $nama_ketua = $this->input->post('nama_ketua');
        $waktu = $this->input->post('waktu');
        $lokasi = $this->input->post('lokasi');
        $nama_dpl = $this->input->post('nama_dpl');
        $pejabat_desa = $this->input->post('pejabat_desa');
        $dpl_terlibat = $this->input->post('dpl_terlibat');
        $dpl_antar = $this->input->post('dpl_antar');
        $dpl_aktif = $this->input->post('dpl_aktif');
        $log_book = $this->input->post('log_book');
        $jadwal_pelaksanaan = $this->input->post('jadwal_pelaksanaan');
        $daftar_harian = $this->input->post('daftar_harian');
        $kekompakan = $this->input->post('kekompakan');
        $permasalahan = $this->input->post('permasalahan');
        $penyediaan_posko = $this->input->post('penyediaan_posko');
        $partisipasi_mas = $this->input->post('partisipasi_mas');
        $desa_butuh = $this->input->post('desa_butuh');
        $prog_unggulan = $this->input->post('prog_unggulan');
        $masalah_desa = implode(',', $this->input->post('masalah_desa',TRUE));
        $harapan = $this->input->post('harapan');
       
        $data = array(
            'id_bio' => $id_bio,
            'judul' => $judul, 
            'nama_ketua' => $nama_ketua,
            'waktu' => $waktu,
            'lokasi' => $lokasi,
            'nama_dpl' => $nama_dpl,
            'pejabat_desa' => $pejabat_desa,
            'dpl_terlibat' => $dpl_terlibat,
            'dpl_antar' => $dpl_antar,
            'dpl_aktif' => $dpl_aktif,
            'log_book' => $log_book,
            'jadwal_pelaksanaan' => $jadwal_pelaksanaan,
            'daftar_harian' => $daftar_harian,
            'kekompakan' => $kekompakan,
            'permasalahan' => $permasalahan,
            'penyediaan_posko' => $penyediaan_posko,
            'partisipasi_mas' => $partisipasi_mas,
            'desa_butuh' => $desa_butuh,
            'prog_unggulan' => $prog_unggulan,
            'masalah_desa' => $masalah_desa,
            'harapan' => $harapan
        );
    
        $where = array(
            'id_bio' => $id_bio
        );
    
        $this->m_forma->update_data($where,$data,'biodata');
        redirect('Forma/index');
    }
}