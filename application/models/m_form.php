<?php

class M_form extends CI_Model{
    function tampil(){
        return $this->db->get('biodata');
    }

    function input($data,$table){
        $this->db->insert($table,$data);
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function edit_data($where,$table){		
        return $this->db->get_where($table,$where);
    }

    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
    }	
    
    function get_kuesioner_list($limit, $start){
        $query = $this->db->get('biodata', $limit, $start);
        return $query;
    }
}