<?php

class M_forma extends CI_Model{
    function tampil(){
        $this->load->library('pagination'); // Load librari paginationnya        
            $query = "SELECT * FROM biodata"; // Query untuk menampilkan semua data siswa        
            $config['base_url'] = base_url('Forma/index');    
            $config['total_rows'] = $this->db->query($query)->num_rows();    
            $config['per_page'] = 5;    
            $config['uri_segment'] = 3;    
            $config['num_links'] = 3;        // Style Pagination    // Agar bisa mengganti stylenya sesuai class2 yg ada di bootstrap    
            $config['first_link']       = 'First';
            $config['last_link']        = 'Last';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';        
             $this->pagination->initialize($config); // Set konfigurasi paginationnya        
             $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;    
             $query .= " LIMIT ".$page.", ".$config['per_page'];        
             $data['limit'] = $config['per_page'];    
             $data['total_rows'] = $config['total_rows'];    
             $data['pagination'] = $this->pagination->create_links(); // Generate link pagination nya sesuai config diatas    
             $data['biodata'] = $this->db->query($query)->result();        
            
             return $data; 

        return $this->db->get('biodata');
    }

    function input($data,$table){
        $this->db->insert($table,$data);
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function edit_data($where,$table){		
        return $this->db->get_where($table,$where);
    }

    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
    }	
    
    function get_kuesioner_list($limit, $start){
        $query = $this->db->get('biodata', $limit, $start);
        return $query;
    }
}