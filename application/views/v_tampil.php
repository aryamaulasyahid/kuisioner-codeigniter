<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quesioner Monitor dan Evaluasi Administrasi KKN UNRAM</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.css">

	
	<script src="<?php echo base_url()?>assets/js/jquery.js"></script>

</head>
<body>

<header>
<nav class="navbar navbar-light" style="background-color: #210756;">
<div class="container-fluid">
	<div class="navbar-header">
	<div class="p-3 mb-2 text-white"><h1>QUESIONER MONITORING DAN EVALUASI ADMINISTRASI KKN UNRAM </h1></div>
	</div>
</div>
</nav>
</header>

<div class="container-fluid">
<div class="rows">
<h2 class="page-header">TABEL HASIL QUESIONER</h2>
	<div class="table-responsive">
	<table class="table table-striped">
		<thead class="thead-light">
		<tr>

			<th>Judul Proposal</th>
			<th>Nama Ketua</th>
			<th>Waktu Pelaksanaan</th>
			<th>Lokasi</th>
			<th>Nama DPL</th>
			<th>Pejabat Desa</th>
			<th>Apakah DPL Terlibat dalam Penyusunan Proposal</th>
			<th>Apakah DPL Mengantar Ke Posko</th>
			<th>Apakah DPL aktif dalam Kegiatan Desa</th>
			<th>Log Book</th>
			<th>Jadwal Program</th>
			<th>Daftar Hadir</th>
			<th>Kekompakan</th>
			<th>Permasalahan</th>
			<th>Penyediaan Posko</th>
			<th>Partisipasi Masyarakat</th>
			<th>Apakah Desa Butuh Kelompok KKN</th>
			<th>Program Unggulan Desa</th>
			<th>Permasalahan Desa</th>
			<th>Harapan Masyarakat</th>
			<th>Proses</th>
		</tr>
		</thead>
		<?php 
		foreach($biodata as $b){ 
		?>
		<tr>

			<td><?php echo $b->judul ?></td>
			<td><?php echo $b->nama_ketua ?></td>
			<td><?php echo $b->waktu ?></td>
			<td><?php echo $b->lokasi ?></td>
			<td><?php echo $b->nama_dpl ?></td>
			<td><?php echo $b->pejabat_desa ?></td>
			<td><?php echo $b->dpl_terlibat ?></td>
			<td><?php echo $b->dpl_antar ?></td>
			<td><?php echo $b->dpl_aktif ?></td>
			<td><?php echo $b->log_book ?></td>
			<td><?php echo $b->jadwal_pelaksanaan ?></td>
			<td><?php echo $b->daftar_harian ?></td>
			<td><?php echo $b->kekompakan ?></td>
			<td><?php echo $b->permasalahan ?></td>
			<td><?php echo $b->penyediaan_posko ?></td>
			<td><?php echo $b->partisipasi_mas ?></td>
			<td><?php echo $b->desa_butuh ?></td>
			<td><?php echo $b->prog_unggulan ?></td>
			<td><?php echo $b->masalah_desa ?></td>
			<td><?php echo $b->harapan ?></td>
			<td>
				<?php echo anchor('Form/edit/'.$b->id_bio,'Edit'); ?>
				<?php echo anchor('Form/hapus/'.$b->id_bio,'Hapus'); ?>
			</td>
		</tr>
		<?php } ?>
	</table>
	</div>

	<button type="button" class="btn btn-link"><?php echo anchor('Form/tambah_data','Tambah Data'); ?></button>
	<div class="row">
        <div class="col">
            <!--Tampilkan pagination-->
            <?php echo $pagination; ?>
        </div>
    </div>
	
</div>
</div>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
</body>
</html>