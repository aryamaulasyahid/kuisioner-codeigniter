<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quesioner Monitor dan Evaluasi Administrasi KKN UNRAM</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.css')?>">
    <script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.min.css')?>">
    <script src="<?php echo base_url('assets/js/datatables.min.js')?>"></script>
	<script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
	</script>

	
	</head>
<body style="background-color: white;">

<header>
<nav class="navbar navbar-light" style="background-color: #553D67;">
<div class="container-fluid">
	<div class="navbar-header">
	<div class="p-3 mb-2 text-white"><h1>QUESIONER MONITORING DAN EVALUASI ADMINISTRASI KKN UNRAM </h1></div>
	</div>
</div>
</nav>
</header>

<div class="container-fluid">
<div class="rows">
<h2 class="page-header p-3 mb-2 text-black">TABEL HASIL QUESIONER</h2>
	<div class="table-responsive">
	<table id="tabel-data" class="table table-bordered" style="background-color: #fff;" >
		<thead class="thead-light">
		<tr>
			<th>Judul Proposal</th>
			<th>Nama Ketua</th>
			<th>Waktu Pelaksanaan</th>
			<th>Lokasi</th>
			<th>Nama DPL</th>
			<th>Pejabat Desa</th>
			<th>Apakah DPL Terlibat dalam Penyusunan Proposal</th>
			<th>Apakah DPL Mengantar Ke Posko</th>
			<th>Apakah DPL aktif dalam Kegiatan Desa</th>
			<th>Log Book</th>
			<th>Jadwal Program</th>
			<th>Daftar Hadir</th>
			<th>Kekompakan</th>
			<th>Permasalahan</th>
			<th>Penyediaan Posko</th>
			<th>Partisipasi Masyarakat</th>
			<th>Apakah Desa Butuh Kelompok KKN</th>
			<th>Program Unggulan Desa</th>
			<th>Permasalahan Desa</th>
			<th>Harapan Masyarakat</th>
			<th>Proses</th>
		</tr>
		</thead>
		<?php    
		if( ! empty($model['biodata'])){ // Jika data pada database tidak sama dengan empty (alias ada datanya)      
			foreach($model['biodata'] as $data){ // Lakukan looping pada variabel siswa dari controller        
			echo "<tr>";        
			echo "<td>".$data->judul."</td>";        
			echo "<td>".$data->nama_ketua."</td>";        
			echo "<td>".$data->waktu."</td>";        
			echo "<td>".$data->lokasi."</td>";        
			echo "<td>".$data->nama_dpl."</td>";
			echo "<td>".$data->pejabat_desa."</td>";
			echo "<td>".$data->dpl_terlibat."</td>";
			echo "<td>".$data->dpl_antar."</td>";
			echo "<td>".$data->dpl_aktif."</td>";
			echo "<td>".$data->log_book."</td>";
			echo "<td>".$data->jadwal_pelaksanaan."</td>";
			echo "<td>".$data->daftar_harian."</td>"; 
			echo "<td>".$data->kekompakan."</td>";
			echo "<td>".$data->permasalahan."</td>"; 
			echo "<td>".$data->penyediaan_posko."</td>"; 
			echo "<td>".$data->partisipasi_mas."</td>";
			echo "<td>".$data->desa_butuh."</td>";
			echo "<td>".$data->prog_unggulan."</td>";
			echo "<td>".$data->masalah_desa."</td>";
			echo "<td>".$data->harapan."</td>";
			echo "<td>".anchor('Forma/hapus/'.$data->id_bio,'<button type="button" class="btn btn-danger">Hapus</button>')."</td>";                               
			echo "</tr>";      
			}    
		}else
		{ // Jika data tidak ada      
			echo "<tr><td colspan='5'>Data tidak ada</td></tr>";    }    ?>
	</table>
	

	</div>
	
	<button type="button" class="btn btn-link"><?php echo anchor('Forma/tambah_data','<button type="button" class="btn btn-primary">Tambah Data</button>'); ?></button>
	<div class="row">
        <div class="col">
            <!--Tampilkan pagination-->
            <?php  echo $model['pagination']; ?>
        </div>
    </div>
	
</div>
</div>

	
<section id="footer" style="background-color: white;">
	<div class="container" >
	<div class="row text-center text-xs-center text-sm-left text-md-left">
		<div class="col-md-12 text-center text-black" >
		<p></p>
			<h5>copyright @2019 </h5>
			<p>Created by : LPPM UNRAM</p>
			</div>
		</div>
	</div>
</section>

<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
</body>
</html>