<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quesioner Monitor dan Evaluasi Administrasi KKN UNRAM</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.css')?>">
    <script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>

	<script>
	$(document).ready(function () {
		swal("Silahkan Diisi !")
	});
	</script>
</head>
<body>
	<center>
		<h1>Quesioner Monitor dan Evaluasi Administrasi</h1>
		<h3>Tambah data baru</h3>
    </center>
    <div class="container">
	<form action="<?php echo base_url(). 'Forma/tambah_aksi'; ?>" method="post">
    <label><h4><u>Silahkan Isi Biodata</u></h4></label>    
        <div class="form-group">
            <label for="id_bio">ID BIODATA :</label>
				<input type="text" class="form-control" id="id_bio" name="id_bio">
        </div>
		<div class="form-group">
            <label for="judul">Judul/Tema :</label>
            <input type="text" class="form-control" id="judul" name="judul">
        </div>
        <div class="form-group">
            <label for="nama_ketua">Nama Ketua KKN :</label>
            <input type="text" class="form-control" id="nama_ketua" name="nama_ketua">
        </div>
        <div class="form-group">
            <label for="waktu">Waktu Pelaksanaan :</label>
            <input type="text" class="form-control" value="45 Hari" id="waktu" name="waktu" placeholder="45 Hari" readonly>
        </div>
        <div class="form-group">
            <label for="lokasi">Lokasi KKN :</label>
            <input type="text" class="form-control" id="lokasi" name="lokasi">
        </div>
        <div class="form-group">
            <label for="nama_dpl">Nama Dosen Pembimbing Lapangan :</label>
            <input type="text" class="form-control" id="nama_dpl" name="nama_dpl">
        </div>
        <div class="form-group">
            <label for="pejabat_desa">Nama Pejabat Desa :</label>
            <input type="text" class="form-control" id="pejabat_desa" name="pejabat_desa">
        </div>

        <label><h4><u>A. Dosen Pembimbing Lapangan</u></h4></label><p>    
        <label for="dpl_terlibat">1. Apakah DPL Terlibat dalam penyusunan proposal KKN :</label>
        <div class="radio">
            <input type="radio" value ="Ya" name="dpl_terlibat">Ya</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak" name="dpl_terlibat">Tidak</label>
        </div>
        <label for="dpl_antar">2. Apakah DPL Ikut Mengantar dan Menyerahkan mahasiswa KKN ke Desa :</label>
        <div class="radio">
            <input type="radio" value ="Ya" name="dpl_antar">Ya</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak" name="dpl_antar">Tidak</label>
        </div>

        <label for="dpl_aktif">3. Apakah DPL Aktif Terlibat dalam Diskusi Pelaksanaan Program Kerja selama KKN :</label>
        <div class="radio">
            <input type="radio" value ="Sangat Aktif" name="dpl_aktif">Sangat Aktif</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Cukup" name="dpl_aktif">Cukup</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Aktif" name="dpl_aktif">Tidak Aktif</label>
        </div>
        <label><h4><u>B. Peserta KKN</u></h4></label><p>    
        <label for="log_book">1. Log Book :</label>
        <div class="radio">
            <input type="radio" value ="Ada" name="log_book">Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Belum Ada" name="log_book">Belum Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Ada" name="log_book">Tidak Ada</label>
        </div>

        <label for="jadwal_pelaksanaan">2. Jadwal Pelaksanaan Program :</label>
        <div class="radio">
            <input type="radio" value ="Ada" name="jadwal_pelaksanaan">Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Belum Ada" name="jadwal_pelaksanaan">Belum Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Ada" name="jadwal_pelaksanaan">Tidak Ada</label>
        </div>

        <label for="daftar_harian">3. Daftar Hadir Harian :</label>
        <div class="radio">
            <input type="radio" value ="Ada" name="daftar_harian">Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Belum Ada" name="daftar_harian">Belum Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Ada" name="daftar_harian">Tidak Ada</label>
        </div>

        <label for="kekompakan">4. Kekompakan</label>
        <div class="radio">
            <input type="radio" value ="Kompak" name="kekompakan">Kompak</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Kurang Kompak" name="kekompakan">Kurang Kompak</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Kompak" name="kekompakan">Tidak Kompak</label>
        </div>
        <div class="form-group">
            <label for="permasalahan">5. Permasalahan yang Dihadapi :</label>
            <textarea class="form-control" id="permasalahan" name="permasalahan" rows="3"></textarea>
        </div>

        <label><h4><u>C. Partisipasi Masyarakat</u></h4></label><p>   
        <label for="penyediaan_posko">1. Penyediaan Posko Gratis :</label>
        <div class="radio">
            <input type="radio" value ="Ada" name="penyediaan_posko">Ada</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Ada" name="penyediaan_posko">Tidak Ada</label>
        </div>
        
        <label for="partisipasi_mas">2. Partisipasi Masyarakat :</label>
        <div class="radio">
            <input type="radio" value ="Tinggi" name="partisipasi_mas">Tinggi</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Sedang" name="partisipasi_mas">Sedang</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Rendah" name="partisipasi_mas">Rendah</label>
        </div>

        <label><h4><u>D. Potensi dan Permasalahan Desa</u></h4></label><p>
        <label for="desa_butuh">1. Apakah Desa membutuhkan mahasiswa KKN pada Periode berikutnya :</label>
        <div class="radio">
            <input type="radio" value ="Butuh" name="desa_butuh">Butuh</label>
        </div>
        <div class="radio">
            <input type="radio" value ="Tidak Butuh" name="desa_butuh">Tidak Butuh</label>
        </div>  
        <label for="masalah_desa">2. Apa Program unggulan desa (lingkari salah satu atau lebih) :</label>
        <div class="checkbox">
            <label><input type="checkbox" value="Pertanian" name="prog_unggulan[]">
            Pertanian</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Pendidikan" name="prog_unggulan[]">Pendidikan</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Ekonomi" name="prog_unggulan[]">Ekonomi</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Wisata" name="prog_unggulan[]">Wisata</label>
        </div>
        <label for="masalah_desa">3. Permasalahan Desa :</label>
        <div class="checkbox">
            <label><input type="checkbox" value="Pertanian tanaman pangan/peternakan/kehutanan/perkebunan" name="masalah_desa[]">
            Pertanian tanaman pangan/peternakan/kehutanan/perkebunan</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Pendidikan" name="masalah_desa[]">Pendidikan</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Ekonomi" name="masalah_desa[]">Ekonomi</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Kesehatan" name="masalah_desa[]">Kesehatan</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Humaniora" name="masalah_desa[]">Humaniora</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Infrastuktur" name="masalah_desa[]">Infrastuktur</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Lingkungan" name="masalah_desa[]">Lingkungan</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" value="Keamanan" name="masalah_desa[]">Keamanan</label>
        </div>
        <div class="form-group">
            <label for="prog_unggulan">4. Apa Harapan bapak/saudara terhadap KKN kedepan :</label>
            <textarea class="form-control" id="harapan" name="harapan" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
	</form>	
    
    <section id="footer" style="background-color: white;">
	<div class="container" >
	<div class="row text-center text-xs-center text-sm-left text-md-left">
		<div class="col-md-12 text-center text-black" >
			<p></p>
            <h5>copyright @2019 </h5>
			<p>Created by : LPPM UNRAM</p>
			</div>
		</div>
	</div>
</section>
</body>
</html>