<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quesioner Monitor dan Evaluasi Administrasi KKN UNRAM</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.css">

</head>
<body>
	<center>
		<h1>Quesioner Monitor dan Evaluasi Administrasi</h1>
		<h3>Tambah data baru</h3>
	</center>
	<form action="<?php echo base_url(). 'Form/tambah_aksi'; ?>" method="post">
		<table style="margin:20px auto;">
            <tr>
            <th>Isi Biodata</th>
            </tr>
			<tr>
				<td>ID BIODATA</td>
				<td><input type="text" name="id_bio"></td>
			</tr>
			<tr>
				<td>Judul</td>
				<td><textarea name="judul" rows="5" col="150"></textarea></td>
			</tr>
			<tr>
				<td>Nama Ketua KKN</td>
				<td><input type="text" name="nama_ketua"></td>
			</tr>
            <tr>
				<td>Waktu</td>
				<td><input type="date" name="waktu"></td>
			</tr>
            <tr>
				<td>Lokasi KKN</td>
				<td><textarea name="lokasi" rows="5" col="150"></textarea></td>
			</tr>
            <tr>
				<td>Nama DPL</td>
				<td><input type="text" name="nama_dpl"></td>
			</tr>
            <tr>
				<td>Nama Pejabat Desa</td>
				<td><input type="text" name="pejabat_desa"></td>
			</tr>
			</tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr>
            <th>A. Dosen Pembimbing Lapangan</th>
            </tr>
            <tr>
				<td>Apakah DPL Terlibat dalam Penyusunan</td>
				<td><input type="radio" name="dpl_terlibat"
                value="Ya"> Ya
                <input type="radio" name="dpl_terlibat"
                value="Tidak">Tidak
                </td>
			</tr>
            <tr>
				<td>Apakah DPL ikut mengantar dan menyerahkan mahasiswa KKN ke Desa</td>
				<td><input type="radio" name="dpl_antar"
                value="Ya"> Ya
                <input type="radio" name="dpl_antar"
                value="Tidak">Tidak
                </td>
			</tr>
            <tr>
				<td>Apakah DPL aktif teribat dalam diskusi pelaksanaan program kerja selama KKN</td>
				<td><input type="radio" name="dpl_aktif"
                value="Sangat Aktif"> Sangat Aktif
                <input type="radio" name="dpl_aktif"
                value="Cukup">Cukup
                <input type="radio" name="dpl_aktif"
                value="Tidak Aktif">Tidak Aktif
                </td>
			</tr>
			
			<tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
			<tr>
            <th>B. Peserta KKN</th>
            </tr>
            <tr>
				<td>Log Book</td>
				<td><input type="radio" name="log_book"
                value="Ada"> Ada
                <input type="radio" name="log_book"
                value="Belum ada">Belum ada
				<input type="radio" name="log_book"
                value="Tidak ada">Tidak ada
                </td>
			</tr>
			<tr>
				<td>Jadwal Pelaksanaan Program</td>
				<td><input type="radio" name="jadwal_pelaksanaan"
                value="Ada"> Ada
                <input type="radio" name="jadwal_pelaksanaan"
                value="Belum ada">Belum ada
				<input type="radio" name="jadwal_pelaksanaan"
                value="Tidak ada">Tidak ada
                </td>
			</tr>
			<tr>
				<td>Daftar hadir harian</td>
				<td><input type="radio" name="daftar_harian"
                value="Ada"> Ada
                <input type="radio" name="daftar_harian"
                value="Belum ada">Belum ada
				<input type="radio" name="daftar_harian"
                value="Tidak ada">Tidak ada
                </td>
			</tr>
			<tr>
				<td>Kekompakan</td>
				<td><input type="radio" name="kekompakan"
                value="Kompak"> Kompak
                <input type="radio" name="kekompakan"
                value="Kurang kompak">Kurang kompak
				<input type="radio" name="kekompakan"
                value="Tidak kompak">Tidak kompak
                </td>
			</tr>
			<tr>
				<td>Permasalahan yang dihadapi</td>
				<td><td><textarea name="permasalahan" rows="5" col="150"></textarea></td>
                </td>
			</tr>
			<tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
			<tr>
            <th>C. Partisipasi Masyarakat</th>
            </tr>
			<tr>
				<td>Penyediaan Posko Gratis</td>
				<td><input type="radio" name="penyediaan_posko"
                value="Ada">Ada
                <input type="radio" name="penyediaan_posko"
                value="Tidak ada">Tidak ada
                </td>
			</tr>
			<tr>
				<td>Partisipasi Masyarakat</td>
				<td><input type="radio" name="partisipasi_mas"
                value="Tinggi">Tinggi
                <input type="radio" name="partisipasi_mas"
                value="Sedang">Sedang
				<input type="radio" name="partisipasi_mas"
                value="Rendah">Rendah
                </td>
			</tr>
			<tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
			<tr>
            <th>D. Potensi dan Permasalahan Desa</th>
            </tr>
			<tr>
				<td>Apakah desa membutuhkan mahasiswa KKN pada periode berikutnya</td>
				<td><input type="radio" name="desa_butuh"
                value="Butuh">Butuh
                <input type="radio" name="desa_butuh"
                value="Tidak butuh">Tidak butuh
                </td>
			</tr>
			<tr>
				<td>Apa Program Unggulan Desa</td>
				<td><td><textarea name="prog_unggulan" rows="5" col="150"></textarea></td>
                </td>
			</tr>
            <tr>
				<td>Permasalahan Desa</td>
				<td>
				<input type="checkbox" name="masalah_desa[]"
                value="Pertanian tanaman pangan/peternakan/kehutanan/perkebunan">Pertanian tanaman pangan/peternakan/kehutanan/perkebunan<p>
                <input type="checkbox" name="masalah_desa[]"
                value="Pendidikan:buta aksara, putus sekolah">Pendidikan:buta aksara, putus sekolah<p>
				<input type="checkbox" name="masalah_desa[]"
                value="Ekonomi:kemiskinan, pengangguran">Ekonomi:kemiskinan, pengangguran<p>
				<input type="checkbox" name="masalah_desa[]"
                value="Kesehatan:gizi butuk, stunting">Kesehatan:gizi butuk, stunting<p>
				<input type="checkbox" name="masalah_desa[]"
                value="Humaniora">Humaniora<p>
				<input type="checkbox" name="masalah_desa[]"
                value="Infrastruktur:jalan raya,tempat ibadah">Infrastruktur:jalan raya,tempat ibadah<p>
				<input type="checkbox" name="masalah_desa[]"
                value="Lingkungan:sampah,air bersih, sanitasi(MCK)">Lingkungan:sampah,air bersih, sanitasi(MCK)<p>
				<input type="checkbox" name="masalah_desa[]"
                value="Keamanan:pencurian,perampokan,perkelahian">Keamanan:pencurian,perampokan,perkelahian
                </td>
			</tr>
			<tr>
				<td>Apa harapan bapak/saudara terhadap KKN Kedepan</td>
				<td><td><textarea name="harapan" rows="5" col="150"></textarea></td>
                </td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Tambah"></td>
			</tr>
		</table>
	</form>	
</body>
</html>